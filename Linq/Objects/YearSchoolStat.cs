﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Objects
{
    public class YearSchoolStat
    {
        public int Year { get; set; }
        public int NumberOfSchools { get; set; }
    }
}
