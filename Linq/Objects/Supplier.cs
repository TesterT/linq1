﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq.Objects
{
    public class Supplier
    {
        public int Id { get; set; }
        public int YearOfBirth { get; set; }
        public string Adress { get; set; }
    }
}
